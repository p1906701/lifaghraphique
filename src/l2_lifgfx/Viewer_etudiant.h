
#ifndef VIEWER_ETUDIANT_H
#define VIEWER_ETUDIANT_H

#include "Viewer.h"



class ViewerEtudiant : public Viewer
{
public:
    ViewerEtudiant();

    int init();
    int render();
    int update( const float time, const float delta );

protected:

    /* -----------------------------------------
     Pour creer un nouvel objet vous devez :

     1. declarer ici dans le fichier Viewer_etudiant.h
     le Mesh,
     la texture si besoin,
     une fonction 'init_votreObjet'
     une fonction 'draw_votreObjet(const Transform& T)'

     --- Rq : regarder comment cela est effectue dans le fichier Viewer.h


     2. Appeler la fonction 'init_votreObjet' dans la fonction 'init' du Viewer_etudiant.cpp
     --- Rq : regarder comment cela est effectue dans le fichier Viewer.cpp


     3. Appeler la fonction 'draw_votreObjet' dans la fonction 'render' du Viewer_etudiant.cpp
     --- Rq : regarder comment cela est effectue dans le fichier Viewer.cpp

      ----------------------------------------- */

    Mesh m_disque;
    Mesh m_cylindre;
    Mesh m_cone;
    Mesh m_sphere;

    void init_sphere();
    void init_cone();
    void init_cylinder();
    void init_disque();
    void init_cube();



    void draw_Pied1(const Transform& T);
    void draw_Pied2(const Transform& T);
    void draw_Jambe1(const Transform& T);
    void draw_Jambe2(const Transform& T);
    void draw_buste(const Transform& T);
    void draw_Bras1(const Transform& T);



};



#endif
