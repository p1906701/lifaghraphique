
#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>

#include "draw.h"        // pour dessiner du point de vue d'une camera
#include "Viewer_etudiant.h"

using namespace std;





/*
 * Exemple de definition de fonction permettant la creation du Mesh de votre Objet.
 */
//void ViewerEtudiant::init_votreObjet()
//{
//}

    //CUBE

    void ViewerEtudiant::init_cube()
    {

        m_cube = Mesh(GL_TRIANGLE_STRIP);


        static float pt[8][3] = { {-1,-1,-1}, {1,-1,-1}, {1,-1,1}, {-1,-1,1}, {-1,1,-1}, {1,1,-1}, {1,1,1}, {-1,1,1} };

        static int f[6][4] = { {0,1,2,3}, {5,4,7,6}, {2,1,5,6}, {0,3,7,4}, {3,2,6,7}, {1,0,4,5} };

        static float n[6][3] = { {0,-1,0}, {0,1,0}, {1,0,0}, {-1,0,0}, {0,0,1}, {0,0,-1} };

        for (int i=0; i<6; i++)
        {
            m_cube.normal(n[i][0], n[i][1], n[i][2]);

            m_cube.vertex( pt[ f[i][0]][0], pt[ f[i][0]][1], pt[ f[i][0]][2] );
            m_cube.vertex( pt[ f[i][1] ][0], pt[ f[i][1] ][1], pt[ f[i][1] ][2] );
            m_cube.vertex( pt[ f[i][3] ][0], pt[ f[i][3] ][1], pt[ f[i][3] ][2] );
            m_cube.vertex( pt[ f[i][2] ][0], pt[ f[i][2] ][1], pt[ f[i][2] ][2] );
            m_cube.restart_strip();
        }
    }

    //DISQUE



    void ViewerEtudiant::init_disque()
    {
        const int div = 25;
        float alpha;
        float step= 2.0 * M_PI / (div);

        m_disque = Mesh( GL_TRIANGLE_FAN );

        m_disque.normal( Vector(0,1,0) );
        m_disque.vertex( Point(0,0,0)  );

        for(int i=0; i<=div; ++i)
            {
                alpha = i * step;
                m_disque.normal( Vector(0,1,0) );
                m_disque.vertex( Point(cos(alpha),0, sin(alpha)) );
            }
    }


    // CYLINDRE


    void ViewerEtudiant::init_cylinder()
    {
        m_cylindre = Mesh(GL_TRIANGLE_STRIP);

        int i;
        const int div = 25;
        float alpha;
        float step= 2.0 * M_PI / (div);



        for(int i=0; i<=div; ++i)
        {
            alpha = i * step;

            m_cylindre.normal( Vector(cos(alpha),0, sin(alpha)) );
            m_cylindre.vertex( Point(cos(alpha),-1, sin(alpha)) );

            m_cylindre.normal( Vector(cos(alpha),0, sin(alpha)) );
            m_cylindre.vertex( Point(cos(alpha), 1, sin(alpha)) );

        }
    }


    //CONE



    void ViewerEtudiant::init_cone()
    {
        const int div = 25;
        float alpha;
        float step= 2.0 * M_PI / (div);

        m_cone= Mesh(GL_TRIANGLE_STRIP);

        for(int i=0;i<=div;++i)
            {
                alpha = i * step;
                m_cone.normal(Vector( cos(alpha)/sqrtf(2.f),1.f/sqrtf(2.f), sin(alpha)/sqrtf(2.f) ));
                m_cone.vertex( Point( cos(alpha),0, sin(alpha) ));
                m_cone.normal(Vector( cos(alpha)/sqrtf(2.f),1.f/sqrtf(2.f), sin(alpha)/sqrtf(2.f) ));
                m_cone.vertex( Point(0, 1, 0) );
            }

    }

    //SPHERE


    void ViewerEtudiant::init_sphere()
    {
        const int divBeta= 16;
        const int divAlpha= divBeta/2;
        int i,j;
        float beta, alpha, alpha2;
        m_sphere= Mesh(GL_TRIANGLE_STRIP);

        for(int i=0; i<divAlpha; ++i)
            {
                alpha= -0.5f * M_PI + float(i) * M_PI / divAlpha;
                alpha2= -0.5f * M_PI + float(i+1) * M_PI / divAlpha;
                for(int j=0; j<=divBeta; ++j)
                    {
                        beta= float(j) * 2.f * M_PI / (divBeta);
                        m_sphere.normal( Vector(cos(alpha)*cos(beta),sin(alpha), cos(alpha)*sin(beta)) );
                        m_sphere.vertex( Point(cos(alpha)*cos(beta),sin(alpha), cos(alpha)*sin(beta)) );
                        m_sphere.normal( Vector(cos(alpha2)*cos(beta), sin(alpha2), cos(alpha2)*sin(beta)) );
                        m_sphere.vertex( Point(cos(alpha2)*cos(beta),sin(alpha2), cos(alpha2)*sin(beta)) );
                    }
                m_sphere.restart_strip();
            }
    }



/*
 * Fonction dans laquelle les initialisations sont faites.
 */
int ViewerEtudiant::init()
{
    Viewer::init();

    m_camera.lookat( Point(0,0,0), 150 );


    /// Appel des fonctions init_votreObjet pour creer les Mesh


    init_cone();
    init_cylinder();
    init_disque();
    init_sphere();
    init_cube();



    /// Chargement des textures


    return 0;
}



/*
 * Exemple de definition de fonction permettant l affichage
 * de 'votreObjet' subissant la Transform T
 */
//void ViewerEtudiant::draw_votreObjet(const Transform& T)
//{
// gl.texture(....);
// gl.model( T );
// gl.draw( m_votreObjet );
//}



void ViewerEtudiant::draw_Pied1(const Transform& T)
{
    Transform Tr = T;

    Tr = Translation(2,0,0) * RotationX(90) * Scale(1,2,1);
    gl.model(Tr);
    gl.draw(m_cylindre);

    Tr = Translation(2,0,2) * RotationX(90);
    gl.model(Tr);
    gl.draw(m_cone);

    Tr = Translation(2,0,-2);
    gl.model(Tr);
    gl.draw(m_sphere);
}

void ViewerEtudiant::draw_Pied2(const Transform& T)
{
    Transform Tr;

    Tr = T * Translation(-2,0,0) * RotationX(90) * Scale(1,2,1);
    gl.model(Tr);
    gl.draw(m_cylindre);

    Tr = T * Translation(-2,0,2) * RotationX(90);
    gl.model(Tr);
    gl.draw(m_cone);

    Tr = T * Translation(-2,0,-2);
    gl.model(Tr);
    gl.draw(m_sphere);
}


void ViewerEtudiant::draw_Jambe1(const Transform& T)
{

    Transform Tr = T;

    Tr = Translation(2,4,-2) * Scale(1,4,1);
    gl.model(Tr);
    gl.draw(m_cylindre);

    Tr = Translation(2,8,-2) * Scale(1,1,1);
    gl.model(Tr);
    gl.draw(m_sphere);

    Tr =Translation(2,12,-6) * RotationX(-45) * Scale(1,6,1);
    gl.model(Tr);
    gl.draw(m_cylindre);


}

void ViewerEtudiant::draw_Jambe2(const Transform& T)
{
    Transform Tr = T;

    Tr = T * Translation(-2,4,-2) * Scale(1,4,1);
    gl.model(Tr);
    gl.draw(m_cylindre);

    Tr = T * Translation(-2,8,-2) * Scale(1,1,1);
    gl.model(Tr);
    gl.draw(m_sphere);

    Tr =T* Translation(-2,11,-5) * RotationX(-45) * Scale(1,4,1);
    gl.model(Tr);
    gl.draw(m_cylindre);

}


void ViewerEtudiant::draw_buste(const Transform& T)
{
    Transform Tr = T;

/*    Tr = Translation(0,26,-10) * Scale(4.5,10,3);
    gl.model(Tr);
    gl.draw(m_cylindre);

    Tr = Translation(0,16,-10) * Scale(4.5,10,3);
    gl.model(Tr);
    gl.draw(m_disque);

    Tr = Translation(0,36,-10) * Scale(4.5,10,3) * RotationX(180);
    gl.model(Tr);
    gl.draw(m_disque);
*/
    Tr = Translation(0,23,-10) * Scale(4.5,10,3);
    gl.model(Tr);
    gl.draw(m_sphere);
}


void ViewerEtudiant::draw_Bras1(const Transform& T)
{
    Transform Tr = T;

    Tr = Translation(2,23,-10) * Scale(4.5,10,3);
    gl.model(Tr);
    gl.draw(m_cylindre);
}



/*
 * Fonction dans laquelle les appels pour les affichages sont effectues.
 */
int ViewerEtudiant::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    manageCameraLight();

    gl.camera(m_camera);


    /// Appel de la fonction render() de la class Viewer
    /// pour que vous voyez apparaitre un premier objet
    /// a supprimer ensuite

    Viewer::render();
    /// Appel des fonctions du type 'draw_votreObjet'


    Transform Tr;
    Transform T;

    Tr =Translation(0,2,-15) * RotationX(45) ;

    draw_Pied1(T);
    draw_Pied2(Tr);
    draw_Jambe1(T);
    draw_Jambe2(Tr);

    draw_buste(T);



    return 1;

}


/*
 * Fonction dans laquelle les mises a jours sont effectuees.
 */
int ViewerEtudiant::update( const float time, const float delta )
{
    // time est le temps ecoule depuis le demarrage de l'application, en millisecondes,
    // delta est le temps ecoule depuis l'affichage de la derniere image / le dernier appel a draw(), en millisecondes.



    return 0;
}


/*
 * Constructeur.
 */

ViewerEtudiant::ViewerEtudiant() : Viewer()
{

}


/*
 * Programme principal.
 */
int main( int argc, char **argv )
{
    ViewerEtudiant v;
    v.run();
    return 0;
}
